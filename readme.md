# Offline IM #

simple IM platform built on top of Yii 1.1 webapp with a bit reorganized directory structure and YiiBooster.

### requirements ###
* php5 & php5-sqlite
* composer

### deployment ###
* clone this repo, `cd im`
* `composer install`
* `protected/yiic migrate up --interactive=0`
* `cd public; php -S 127.0.0.1:8000`
* [http://127.0.0.1:8000](http://127.0.0.1:8000)
* you can login with users `test1@mail.com:test1`, `test2@mail.com:test2`


### todos ###

* class diagram
* more security
    * RBAC
    * CSRF
    * hashed passwords storage
    * captcha
* inbox/sent
* caching
* namespaces
* ajax
    * validation
    * messaging itself
* better UI design
	* more icons
* tests
* unused files & comments cleanup