<?php

/**
 * Class UserController
 */
class UserController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['index', 'list', 'view', 'register', 'login', 'logout'],
				'users' => ['*'],
			],
			[
				'allow',
				'actions' => ['update'],
				'users' => ['@'],
			],
			[
				'deny',  // deny all users
				'users' => ['*'],
			],
		];
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);

		$this->render('view', [
			'model' => $model,
		]);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		$model = $this->loadModel(Yii::app()->user->id);

		if (isset($_POST[User::class])) {
			$model->attributes = $_POST[User::class];
			if ($model->save()) {
				$this->redirect(['view', 'id' => $model->id]);
			}
		}

		$this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Lists all models.
	 */
	public function actionList()
	{
		$dataProvider = new CActiveDataProvider(User::class);
		$this->render('list', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 *
	 */
	public function actionIndex()
	{
		$this->redirect('/user/list');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param $id
	 * @return User
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = User::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'The requested page does not exist.');
		}
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * Check if user in current user's contact list
	 * @return bool
	 */
	public static function isInMyContactList($contactId)
	{
		return Contact::model()
			->count('user_id = :user_id AND contact_id = :contact_id', [
				'user_id' => Yii::app()->user->id,
				'contact_id' => $contactId,
			]) == 1;
	}

	/**
	 * User registration
	 */
	public function actionRegister()
	{
		$model = new User('register');
		if (isset($_POST[User::class])) {
			$model->attributes = $_POST[User::class];
			if ($model->save()) {
				$this->redirect(['update', 'id' => $model->id]);
			}
		}
		$this->render('register', [
			'model' => $model,
		]);
	}


	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model = new LoginForm;

		// if it is ajax validation request
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate() && $model->login()) {
				$this->redirect(Yii::app()->user->returnUrl);
			}
		}
		// display the login form
		$this->render('login', ['model' => $model]);
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
