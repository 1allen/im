<?php

/**
 * Class ContactController
 */
class ContactController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['index', 'list', 'update'],
				'users' => ['@'],
			],
			[
				'deny',
				'users' => ['*'],
			],
		];
	}

	/**
	 * Index & redirect to list
	 */
	public function actionIndex()
	{
		$this->redirect('list');
	}

	/**
	 * Contact list
	 */
	public function actionList()
	{
		$dataProvider = new CActiveDataProvider(Contact::class, [
			'criteria' => [
				'condition' => 'user_id = :user_id',
				'params' => [
					'user_id' => Yii::app()->user->id,
				],
				'with' => [
					'user',
					'contact',
				],
			]
		]);
		$this->render('list', [
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Contact list manipulations action
	 */
	public function actionUpdate()
	{
		switch ($_POST['action']) {
			case 'add':
				$contact = new Contact;
				if (isset($_POST['contact_id'])) {
					$contact->user_id = Yii::app()->user->id;
					$contact->contact_id = (int)$_POST['contact_id'];
					if ($contact->save()) {
						$this->redirect(['/user/view', 'id' => $contact->contact_id]);
					} else {
						throw new CHttpException(500, 'error adding contact');
					}
				}
				break;
			case 'remove':
				$contact = Contact::model()->find('user_id = :user_id AND contact_id = :contact_id', [
					'user_id' => Yii::app()->user->id,
					'contact_id' => (int)$_POST['contact_id'],
				]);
				if (!$contact) {
					throw new CHttpException(500, 'contact not found in your contact list');
				}
				if ($contact->delete()) {
					$this->redirect('/user/view/' . $contact->contact_id);
				} else {
					throw new CHttpException(500, 'error deleting contact');
				}
				break;
			default:
				throw new CHttpException('404', 'action not found');
				break;
		}

	}

}
