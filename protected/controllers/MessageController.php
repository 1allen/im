<?php

/**
 * Class MessageController
 */
class MessageController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return [
			'accessControl',
		];
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return [
			[
				'allow',
				'actions' => ['index', 'user', 'list', 'view'],
				'users' => ['@'],
			],
			[
				'deny',
				'users' => ['*'],
			],
		];
	}

	/**
	 * Index & redirect to list
	 */
	public function actionIndex()
	{
		$this->redirect('list');
	}

	/**
	 * Send message to user
	 */
	public function actionUser($id)
	{
		if (isset($id)) {
			// oops!
			Yii::import('application.controllers.UserController');
			$isInMyContactList = UserController::isInMyContactList((int)$id);

			$message = new Message;
			if (isset($_POST[Message::class]) && $isInMyContactList) {
				$message->attributes = $_POST[Message::class];
				$message->sender_id = Yii::app()->user->id;
				$message->recipient_id = (int)$id;
				if ($message->save()) {
					$this->redirect('/message/');
				}
			}

			$this->render('user', [
				'model' => $message,
				'inMyContactList' => $isInMyContactList,
			]);
		} else {
			throw new CHttpException(404, 'recipient is not specified');
		}
	}

	/**
	 * Messages list
	 */
	public function actionList()
	{
		$messages = Message::listUsersMessages(Yii::app()->user->id);

		$this->render('list', [
			'messages' => $messages
		]);
	}

	/**
	 * View message
	 */
	public function actionView($id)
	{
		$message = Message::model()->findByPk($id);

		// cant read message that's not mine
		if (!$message->isRelatedToUser(Yii::app()->user->id)) {
			throw new CHttpException(403, 'The message you`re trying to view is not yours');
		}

		// only recipient can mark message as viewed
		if (!$message->isFromUser(Yii::app()->user->id)) {
			$message->markAsRead();
		}

		$this->render('view', [
			'message' => $message,
		]);
	}
}
