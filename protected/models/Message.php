<?php

/**
 * This is the model class for table "messages".
 *
 * The followings are the available columns in table 'messages':
 * @property integer $id
 * @property integer $sender_id
 * @property integer $recipient_id
 * @property string $text
 * @property string $date
 * @property boolean $read
 */
class Message extends CActiveRecord
{
	/**
	 * @var string readable date
	 */
	public $date_readable;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'messages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return [
			['sender_id, recipient_id, text, date', 'required'],
			['sender_id, recipient_id', 'numerical', 'integerOnly' => true],

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			['id, sender_id, recipient_id, text, date, read', 'safe', 'on' => 'search'],
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'sender' => [self::HAS_ONE, User::class, ['id' => 'sender_id']],
			'recipient' => [self::HAS_ONE, User::class, ['id' => 'recipient_id']],
		];
	}

	/**
	 * @inheritdoc
	 */
	protected function beforeSave()
	{
		// save date as timastamp
		$this->date = time();
		return parent::beforeSave();
	}

	/**
	 * @inheritdoc
	 */
	protected function afterFind()
	{
		// get readable date from timestamp
		$this->date_readable = Yii::app()->dateFormatter->formatDateTime($this->date);
		parent::afterFind();
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'sender_id' => 'Sender',
			'recipient_id' => 'Recipient',
			'text' => 'Text',
			'date' => 'Date',
			'date_readable' => 'Date',
			'read' => 'Read',
		];
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('sender_id', $this->sender_id);
		$criteria->compare('recipient_id', $this->recipient_id);
		$criteria->compare('text', $this->text, true);
		$criteria->compare('date', $this->date, true);
		$criteria->compare('read', $this->read);

		return new CActiveDataProvider($this, [
			'criteria' => $criteria,
		]);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Message the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Returns unread messages count for user
	 * @param $userId
	 * @return string
	 */
	public static function unreadCount($userId)
	{
		return self::model()->count('recipient_id = :user_id AND read = 0', [
			'user_id' => $userId,
		]);
	}

	/**
	 * DataProvider for messages related to user
	 * @param $userId
	 * @return CActiveDataProvider
	 */
	public static function listUsersMessages($userId)
	{
		return new CActiveDataProvider(self::class, [
			'criteria' => [
				'condition' => 'sender_id = :user_id OR recipient_id = :user_id',
				'params' => [
					'user_id' => $userId,
				],
				'with' => [
					'sender',
					'recipient',
				]
			]
		]);
	}

	/**
	 * Marks current message as read
	 * @return bool result
	 */
	public function markAsRead()
	{
		if (!$this->isNewRecord) {
			$this->read = 1;
			return $this->save('read');
		}
		return false;
	}

	/**
	 * Return excerpt for Message text ~20% of original length
	 * @return string
	 */
	public function getExcerpt()
	{
		if (strlen($this->text) > 10) {
			return substr($this->text, 0, floor(strlen($this->text) * 0.2)) . '...';
		}

		return $this->text;
	}

	/**
	 * Is message from specified user
	 * @param $userId
	 * @return bool
	 */
	public function isFromUser($userId)
	{
		return $this->sender_id == $userId;
	}

	/**
	 * Is current message from or to specified user
	 * @param $userId
	 * @return bool
	 */
	public function isRelatedToUser($userId)
	{
		return $this->recipient_id == $userId || $this->sender_id == $userId;
	}
}
