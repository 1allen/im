<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * @var int user id
	 */
	private $_id;

	/**
	 * @inheritdoc
	 */
	public function authenticate()
	{
		$user = $this->loadModel();

		if (!$user) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} elseif ($user->password !== $this->password) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		} else {
			$this->errorCode = self::ERROR_NONE;
			$this->_id = $user->id;
		}
		return !$this->errorCode;
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 * @return User
	 */
	private function loadModel()
	{
		return User::model()
			->find('email = :email', ['email' => $this->username]);
	}


}
