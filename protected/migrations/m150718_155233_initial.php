<?php

class m150718_155233_initial extends CDbMigration
{
	public function up()
	{
		$tableOptions = '';
		$this->createTable('users', [
			'id' => 'pk',
			'username' => 'string NOT NULL',
			'password' => 'string NOT NULL',
			'email' => 'string NOT NULL',
			'status_text' => 'string NULL',
			'online_status_id' => 'int NOT NULL DEFAULT 0',
		], $tableOptions);

		$this->createTable('statuses', [
			'id' => 'pk',
			'name' => 'string NOT NULL',
		], $tableOptions);

		$this->createTable('contacts', [
			'id' => 'pk',
			'user_id' => 'int NOT NULL',
			'contact_id' => 'int NOT NULL',
		], $tableOptions);

		$this->createTable('messages', [
			'id' => 'pk',
			'sender_id' => 'int NOT NULL',
			'recipient_id' => 'int NOT NULL',
			'text' => 'text NOT NULL',
//			date seems to be unsupported there with sqlite
//			'date' => 'datetime default current_timestamp',
			'date' => 'int NOT NULL',
			'read' => 'bool NOT NULL DEFAULT 0'
		], $tableOptions);


		$this->insertMultiple('statuses', [
			[
				'id' => 0,
				'name' => 'Offline',
			],
			[
				'id' => 1,
				'name' => 'Online',
			],
		]);

		$this->insertMultiple('users', [
			[
				'username' => 'test1',
				'password' => 'test1',
				'email' => 'test1@mail.com',
			],
			[
				'username' => 'test2',
				'password' => 'test2',
				'email' => 'test2@mail.com',
			],
		]);
	}

	public function down()
	{
		echo "m150718_155233_initial does not support migration down.\n";
		return false;
	}
}
