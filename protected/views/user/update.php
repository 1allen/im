<?php
$this->breadcrumbs = [
	'Users' => ['/user'],
	$model->id => ['view', 'id' => $model->id],
	'Update',
];

?>

<h1>Update User <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>
