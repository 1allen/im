<?php
$this->breadcrumbs = [
	'Users' => ['/user'],
	'Register',
];

?>

<h1>Register new user</h1>

<?php echo $this->renderPartial('_register_form', ['model' => $model]); ?>
