<?php
/**
 * @var UserController $this
 * @var CActiveDataProvider $dataProvider
 */

$this->breadcrumbs = [
	'Users',
];
?>

<h1>Users</h1>

<?php $this->widget('booster.widgets.TbGridView', [
	'dataProvider' => $dataProvider,
	'template' => '{items}',
	'columns' => [
		[
			'name' => 'username',
			'value' => function ($data) {
				return CHtml::link($data['username'], '/user/view/' . $data['id']);
			},
			'type' => 'html',
		],

	],
]);
