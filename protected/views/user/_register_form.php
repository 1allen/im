<?php
/**
 * @var UserController $this
 * @var User $model
 * @var TbActiveForm $form
 */
$form = $this->beginWidget('booster.widgets.TbActiveForm', [
	'id' => 'user-form',
	'enableAjaxValidation' => false,
]); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'username', ['widgetOptions' => ['htmlOptions' => ['class' => 'span5', 'maxlength' => 255]]]); ?>

<?php echo $form->passwordFieldGroup($model, 'password', ['widgetOptions' => ['htmlOptions' => ['class' => 'span5', 'maxlength' => 255]]]); ?>

<?php echo $form->passwordFieldGroup($model, 'password_confirm', ['widgetOptions' => ['htmlOptions' => ['class' => 'span5', 'maxlength' => 255]]]); ?>

<?php echo $form->textFieldGroup($model, 'email', ['widgetOptions' => ['htmlOptions' => ['class' => 'span5', 'maxlength' => 255]]]); ?>

<?php echo $form->textFieldGroup($model, 'status_text', ['widgetOptions' => ['htmlOptions' => ['class' => 'span5', 'maxlength' => 255]]]); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => 'Register',
	]); ?>
</div>

<?php $this->endWidget(); ?>
