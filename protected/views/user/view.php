<?php
/**
 * @var UserController $this
 * @var User $model
 */
$this->breadcrumbs = [
	'Users' => ['/user'],
	$model->id,
];

?>

<h1>View User #<?php echo $model->id; ?></h1>
<?php
// if I am on myself's page
if (Yii::app()->user->id != $model->id) {
	echo CHtml::form('/contact/update');
	echo CHtml::hiddenField('contact_id', $model->id);
	if (!$this::isInMyContactList($model->id)) {
		echo CHtml::hiddenField('action', 'add');
		echo CHtml::submitButton('Add') . ' from my contact list';

	} else {
		echo CHtml::hiddenField('action', 'remove');
		echo CHtml::submitButton('Remove') . ' to my contact list';
		echo '<br>';
		echo CHtml::link('Write a message', '/message/user/' . $model->id) . ' to user';
	}
	echo CHtml::endForm();
} else {
	echo '<div class="alert alert-info" role="alert">it`s me!<br>' . CHtml::link('edit', '/user/update/') . ' my profile</div>';
}
?>

<?php $this->widget('booster.widgets.TbDetailView', [
	'data' => $model,
	'attributes' => [
		'id',
		'username',
//		'password',
		'email',
		'status_text',
		[
			'name' => 'online_status_id',
			'value' => function ($data) {
				return $data->online_status->name;
			}
		],
		[
			'name' => 'Unread count',
			'value' => function ($data) {
				return Message::unreadCount($data->id);
			}
		]
	],
]); ?>
