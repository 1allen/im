<?php
/* @var $this UserController */
/* @var $model LoginForm */
/* @var $form TbActiveForm */

$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = [
	'Login',
];
?>

<h1>Login</h1>

<p>Please fill out the following form with your login credentials:</p>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', [
	'id' => 'login-form',
	'enableClientValidation' => true,
	'clientOptions' => [
		'validateOnSubmit' => true,
	],
]); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'email'); ?>

<?php echo $form->passwordFieldGroup($model, 'password'); ?>

<?php echo $form->checkboxGroup($model, 'rememberMe'); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', [
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => 'Login',
	]); ?>
</div>

<?php $this->endWidget(); ?>
