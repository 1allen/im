<?php
/**
 * @var MessageController $this
 * @var Message $model
 * @var bool $inMyContactList
 */
$this->breadcrumbs = [
	'Messages' => ['/message'],
	'New messsage',
];

?>
<h1>New message</h1>

<?php if ($inMyContactList): ?>

<?php echo $this->renderPartial('_form', ['model' => $model]); ?>

<?php else: ?>
	<div class="alert alert-danger" role="alert"><?php echo CHtml::link('User', '/user/view/' . (int)$this->actionParams['id']);?> is not in your contact list!</div>
<?php endif; ?>
