<?php
/**
 * @var MessageController $this
 * @var Message $message
 */
$this->breadcrumbs = [
	'Messages' => ['/message'],
	'View messsage',
];

?>
<h1>View message</h1>

<?php $this->widget('booster.widgets.TbDetailView', [
	'data' => $message,
	'attributes' => [
		[
			'name' => 'from',
			'value' => function ($data) {
				return CHtml::link($data->sender->username, '/user/view/' . $data->sender_id);
			},
			'type' => 'html',
		],
		[
			'name' => 'to',
			'value' => function ($data) {
				return CHtml::link($data->recipient->username, '/user/view/' . $data->recipient_id);
			},
			'type' => 'html',
		],
		[
			'name' => 'date_readable',
			'value' => function ($data) {
				return CHtml::link($data->date_readable, '/message/view/' . $data->id);
			},
			'type' => 'html',
		],
		'text',
	],
]); ?>
