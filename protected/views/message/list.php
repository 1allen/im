<?php
/**
 * @var MessageController $this
 * @var CActiveDataProvider $messages
 */
$this->breadcrumbs = [
	'Messages' => ['/message'],
	'Messages list',
];
Yii::app()->clientScript
	->registerCssFile(Yii::app()->request->baseUrl . '/css/styles.css');

?>
	<h1>Message list</h1>

<?php $this->widget('booster.widgets.TbGridView', [
	'dataProvider' => $messages,
	'template' => '{items}',
	'rowCssClassExpression' => '($data->read) ? "" : "bold"',
	'columns' => [
		[
			'name' => 'direction',
			'value' => function ($data) {
				return ($data->sender_id == Yii::app()->user->id) ? 'outgoing' : 'incoming';
			},
		],
		[
			'name' => 'from',
			'value' => function ($data) {
				return CHtml::link($data->sender->username, '/user/view/' . $data->sender_id);
			},
			'type' => 'html',
		],
		[
			'name' => 'to',
			'value' => function ($data) {
				return CHtml::link($data->recipient->username, '/user/view/' . $data->recipient_id);
			},
			'type' => 'html',
		],
		[
			'name' => 'date_readable',
			'value' => function ($data) {
				return CHtml::link($data->date_readable, '/message/view/' . $data->id);
			},
			'type' => 'html',
		],
		[
			'name' => 'text',
			'value' => function ($data) {
				return CHtml::link($data->excerpt, '/message/view/' . $data->id);
			},
			'type' => 'html',
		],
	],
]);
