<?php
/**
 * @var MessageController $this
 * @var Message $model
 * @var TbActiveForm $form
 */
$form = $this->beginWidget('booster.widgets.TbActiveForm', [
	'id' => 'message-form',
	'enableAjaxValidation' => false,
]); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textAreaGroup($model, 'text', ['widgetOptions' => ['htmlOptions' => ['rows' => 6, 'cols' => 50, 'class' => 'span8']]]); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', [
			'buttonType' => 'submit',
			'context' => 'primary',
			'label' => 'Write',
		]); ?>
	</div>

<?php $this->endWidget(); ?>
