<?php
/**
 * @var $this ContactController
 * @var CActiveDataProvider $dataProvider
 */


$this->breadcrumbs = [
	'Users',
];
?>

<h1>Contact list</h1>

<?php $this->widget('booster.widgets.TbGridView', [
	'dataProvider' => $dataProvider,
	'template' => '{items}',
	'columns' => [
		[
			'name' => 'contact_id',
			'value' => function ($data) {
				return CHtml::link($data->contact->username, '/user/view/' . $data->contact_id);
			},
			'type' => 'html',
		],
		[
			'value' => function ($data) {
				return CHtml::link('message', '/message/user/' . $data->contact_id);
			},
			'type' => 'html',
		]

	],
]); ?>
