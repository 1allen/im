<?php
/**
 * @var Controller $this
 */

$this->widget(
	'booster.widgets.TbNavbar',
	[
		'type' => 'inverse',
		'brand' => Yii::app()->params['name'],
		'brandUrl' => '/',
		'collapse' => true,
		'fixed' => false,
		'items' => [
			[
				'class' => 'booster.widgets.TbMenu',
				'type' => 'navbar',
				'encodeLabel' => false,
				'items' => [
					[
						'label' => 'Home',
						'url' => ['/site/index'],
					],
					[
						'label' => 'Users',
						'url' => ['/user/list'],
					],
					[
						'label' => 'Contact list',
						'url' => ['/contact/list'],
						'visible' => !Yii::app()->user->isGuest,
					],
					[
						'label' => 'Message list <span class="badge" title="Unread messages">' . CHtml::encode(Message::unreadCount(Yii::app()->user->id)) . '</span>',
						'url' => ['/message/list'],
						'visible' => !Yii::app()->user->isGuest,
					],
				],
			],
			[
				'class' => 'booster.widgets.TbMenu',
				'type' => 'navbar',
				'htmlOptions' => [
					'class' => 'pull-right',
				],
				'items' => [
					[
						'label' => 'Login',
						'url' => [Yii::app()->user->loginUrl],
						'visible' => Yii::app()->user->isGuest,
					],
					[
						'label' => 'Register',
						'url' => ['/user/register'],
						'visible' => Yii::app()->user->isGuest,
					],
					[
						'label' => 'User ' . Yii::app()->user->name,
						'visible' => !Yii::app()->user->isGuest,
						'items' => [
							[
								'label' => 'Edit profile',
								'url' => ['/user/update'],
							],
							[
								'label' => 'Logout',
								'url' => ['/user/logout'],
							],
						],
					],
				],
			]
		],
	]
);
