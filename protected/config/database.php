<?php

// This is the database connection configuration.
return [
	'connectionString' => 'sqlite:' . __DIR__ . '/../data/db.sqlite3',
	// uncomment the following lines to use a MySQL database
	/*
	'connectionString' => 'mysql:host=localhost;dbname=testdrive',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => '',
	'charset' => 'utf8',
	*/
];
