<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return [
	'basePath' => __DIR__ . DIRECTORY_SEPARATOR . '..',
	'name' => 'Offline IM',

	// preloading 'log' component
	'preload' => [
		'log',
		'booster'
	],

	// autoloading model and component classes
	'import' => [
		'application.models.*',
		'application.components.*',
	],

	'modules' => [
		// uncomment the following to enable the Gii tool
//		'gii' => [
//			'class' => 'system.gii.GiiModule',
//			'password' => 'qwerty',
//			// If removed, Gii defaults to localhost only. Edit carefully to taste.
//			'ipFilters' => ['127.0.0.1', '::1'],
//			'generatorPaths' => [
//				'booster.gii'
//			],
//		],
	],

	// application components
	'components' => [
		'booster' => [
			'class' => 'vendor.clevertech.yii-booster.src.components.Booster',
		],

		'user' => [
			// enable cookie-based authentication
			'allowAutoLogin' => true,
			'loginUrl' => '/user/login',
		],

		// uncomment the following to enable URLs in path-format
		'urlManager' => [
			'urlFormat' => 'path',
			'showScriptName' => false,
			'urlSuffix' => '/',
			'rules' => [
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			],
		],

		// database settings are configured in database.php
		'db' => require(__DIR__ . '/database.php'),

		'errorHandler' => [
			// use 'site/error' action to display errors
			'errorAction' => 'site/error',
		],

		'log' => [
			'class' => 'CLogRouter',
			'routes' => [
				[
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				],
				[
					'class' => 'vendor.malyshev.yii-debug-toolbar.yii-debug-toolbar.YiiDebugToolbarRoute',
					// Access is restricted by default to the localhost
					'ipFilters' => ['127.0.0.1'],
					'enabled' => YII_DEBUG,
				],
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			],
		],
	],

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params' => [

	],
];
